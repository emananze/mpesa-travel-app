package mz.mpesa.travelapp.service.ws;

import mz.mpesa.travelapp.service.model.Indicator;

import java.util.List;

public interface IndicatorService {

    List<Indicator> listIndicators(String countryCode);

}
