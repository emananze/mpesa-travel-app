package mz.mpesa.travelapp.service.model.forecast;

import java.io.Serializable;

public class Cloud implements Serializable {

    private static final long serialVersionUID = 1L;

    private int all;

    public int getAll() {
        return all;
    }

    public void setAll(int all) {
        this.all = all;
    }

    @Override
    public String toString() {
        return "Cloud{" +
                "all=" + all +
                '}';
    }
}
