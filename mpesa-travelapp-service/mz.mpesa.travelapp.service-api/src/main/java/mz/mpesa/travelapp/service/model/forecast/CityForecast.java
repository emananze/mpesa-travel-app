package mz.mpesa.travelapp.service.model.forecast;

import jdk.dynalink.linker.LinkerServices;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public class CityForecast implements Serializable {

    private static final long serialVersionUID = 1L;

    private String base;
    private long visibility;
    private long Date;
    private int timezone;
    private long id;
    private String name;
    private int cod;
    private Cloud cloud;
    private Coord coord;
    private Main main;
    private Sys sys;
    private List<Weather> weather;
    private Wind wind;

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public long getVisibility() {
        return visibility;
    }

    public void setVisibility(long visibility) {
        this.visibility = visibility;
    }

    public long getDate() {
        return Date;
    }

    public void setDate(long date) {
        Date = date;
    }

    public int getTimezone() {
        return timezone;
    }

    public void setTimezone(int timezone) {
        this.timezone = timezone;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCod() {
        return cod;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }

    public Cloud getCloud() {
        return cloud;
    }

    public void setCloud(Cloud cloud) {
        this.cloud = cloud;
    }

    public Coord getCoord() {
        return coord;
    }

    public void setCoord(Coord coord) {
        this.coord = coord;
    }

    public Main getMain() {
        return main;
    }

    public void setMain(Main main) {
        this.main = main;
    }

    public Sys getSys() {
        return sys;
    }

    public void setSys(Sys sys) {
        this.sys = sys;
    }

    public List<Weather> getWeather() {
        return weather;
    }

    public void setWeather(List<Weather> weather) {
        this.weather = weather;
    }

    public Wind getWind() {
        return wind;
    }

    public void setWind(Wind wind) {
        this.wind = wind;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CityForecast that = (CityForecast) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "ForecastItem{" +
                "base='" + base + '\'' +
                ", visibility=" + visibility +
                ", Date=" + Date +
                ", timezone=" + timezone +
                ", id=" + id +
                ", name='" + name + '\'' +
                ", cod=" + cod +
                ", cloud=" + cloud +
                ", coord=" + coord +
                ", main=" + main +
                ", sys=" + sys +
                ", weather=" + weather +
                ", wind=" + wind +
                '}';
    }
}
