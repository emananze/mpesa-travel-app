package mz.mpesa.travelapp.service.exception;

public class TravelServiceException extends RuntimeException{

    public TravelServiceException(String message) {
        super(message);
    }

    public TravelServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
