package mz.mpesa.travelapp.service.ws;

import mz.mpesa.travelapp.service.model.forecast.CityForecast;

public interface ForecastService {

    CityForecast getCityForecast(String city);

}
