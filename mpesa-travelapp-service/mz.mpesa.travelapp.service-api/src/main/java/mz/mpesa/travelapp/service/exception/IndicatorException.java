package mz.mpesa.travelapp.service.exception;

public class IndicatorException extends RuntimeException{

    public IndicatorException(String message) {
        super(message);
    }

    public IndicatorException(String message, Throwable cause) {
        super(message, cause);
    }
}
