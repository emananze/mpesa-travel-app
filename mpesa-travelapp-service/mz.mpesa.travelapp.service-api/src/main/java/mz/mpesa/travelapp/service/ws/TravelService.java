package mz.mpesa.travelapp.service.ws;

import mz.mpesa.travelapp.service.model.CityTravelDetail;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("travel")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface TravelService {

    @GET
    @Path("{cityName}")
    CityTravelDetail getCityTravelDetail(@PathParam("cityName") String cityName);

}
