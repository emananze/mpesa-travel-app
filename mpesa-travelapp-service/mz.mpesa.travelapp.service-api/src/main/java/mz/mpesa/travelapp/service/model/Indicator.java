package mz.mpesa.travelapp.service.model;

import java.io.Serializable;
import java.util.Objects;

public class Indicator implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;
    private int year;
    private long value;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public long getValue() {
        return value;
    }

    public void setValue(long value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Indicator indicator = (Indicator) o;
        return Objects.equals(id, indicator.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Indicator{" +
                "id='" + id + '\'' +
                ", year=" + year +
                ", value=" + value +
                '}';
    }
}
