package mz.mpesa.travelapp.service.model;

import mz.mpesa.travelapp.service.model.forecast.CityForecast;

import java.io.Serializable;
import java.util.List;

public class CityTravelDetail implements Serializable {

    private static final long serialVersionUID = 1L;

    private CityForecast forecast;
    private List<Indicator> indicators;
    private Currency currency;

    public CityForecast getForecast() {
        return forecast;
    }

    public void setForecast(CityForecast forecast) {
        this.forecast = forecast;
    }

    public List<Indicator> getIndicators() {
        return indicators;
    }

    public void setIndicators(List<Indicator> indicators) {
        this.indicators = indicators;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    @Override
    public String toString() {
        return "CityTravelDetail{" +
                "forecast=" + forecast +
                ", indicators=" + indicators +
                ", currency=" + currency +
                '}';
    }
}
