package mz.mpesa.travelapp.service.ws;

import mz.mpesa.travelapp.service.model.Currency;

public interface CurrencyService {

    Currency getCurrency(String countryCode);

}
