package mz.mpesa.travelapp.service.provider;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import mz.mpesa.travelapp.service.model.Currency;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class CurrencyProvider {

    public Currency getCurrency(String countryCode) throws Exception {
        Currency currency = new Currency();
        String currencyCode = this.getCurrencyByCountryCode(countryCode);
        currency.setCode(currencyCode);
        double value = this.getExchangeRate(currencyCode);
        currency.setValue(value);
        return currency;
    }


    private String getCurrencyByCountryCode(String countryCode) throws Exception {
        URI targetURI = new URI(System.getProperty("travel.app.currency.by.country").replace("{country-code}", countryCode));
        HttpRequest httpRequest = HttpRequest.newBuilder().uri(targetURI).GET().build();
        HttpClient httpClient = HttpClient.newHttpClient();
        HttpResponse<String> response = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
        JsonElement jsonElement = JsonParser.parseString(response.body());
        JsonArray jsonArray = jsonElement.getAsJsonArray();
        JsonElement element = jsonArray.get(0);
        String text = element.getAsJsonObject().get("currencies").toString();
        return text.split(":")[0].replace("{\"", "").replace("\"", "");
    }

    private double getExchangeRate(String currencyCode) throws Exception {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        String url = System.getProperty("travel.app.currency.exchange").
                replace("{current_date}", format.format(new Date())).replace("{currency_code}", currencyCode);

        URI targetURI = new URI(url);

        HttpRequest httpRequest = HttpRequest.newBuilder().uri(targetURI).GET().build();

        HttpClient httpClient = HttpClient.newHttpClient();

        HttpResponse<String> response = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());

        JsonElement element = JsonParser.parseString(response.body()).getAsJsonObject().get("rates");

        return Double.parseDouble(element.toString().split(":")[1].replace("}", ""));

    }

}
