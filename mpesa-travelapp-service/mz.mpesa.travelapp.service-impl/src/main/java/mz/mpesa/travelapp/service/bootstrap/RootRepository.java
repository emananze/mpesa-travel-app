package mz.mpesa.travelapp.service.bootstrap;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("/")
public class RootRepository {

    @GET
    public Response response(){
        return Response.ok("hello world").build();
    }

}