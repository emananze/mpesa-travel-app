package mz.mpesa.travelapp.service.ws;

import mz.mpesa.travelapp.service.exception.CurrencyException;
import mz.mpesa.travelapp.service.model.Currency;
import mz.mpesa.travelapp.service.provider.CurrencyProvider;

import javax.inject.Inject;

public class CurrencyServiceImpl implements CurrencyService {

    @Inject
    private CurrencyProvider currencyProvider;

    @Override
    public Currency getCurrency(String countryCode) {
        try {
            return this.currencyProvider.getCurrency(countryCode);
        } catch (Exception exception) {
            throw new CurrencyException("Error occurred while getting currency data from provider API", exception);
        }
    }
}
