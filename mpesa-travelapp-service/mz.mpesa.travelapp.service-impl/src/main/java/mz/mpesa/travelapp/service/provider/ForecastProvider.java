package mz.mpesa.travelapp.service.provider;

import com.google.gson.Gson;
import mz.mpesa.travelapp.service.model.forecast.CityForecast;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class ForecastProvider {

    public CityForecast getForecast(String cityName) throws Exception {
        URI targetURI = new URI(System.getProperty("travel.app.forecast.url").replace("{cityName}", cityName));
        HttpRequest httpRequest = HttpRequest.newBuilder().uri(targetURI).GET().build();
        HttpClient httpClient = HttpClient.newHttpClient();
        HttpResponse<String> response = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
        Gson gson = new Gson();
        return gson.fromJson(response.body(), CityForecast.class);
    }

}
