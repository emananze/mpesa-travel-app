package mz.mpesa.travelapp.service.provider;

import com.google.gson.*;
import mz.mpesa.travelapp.service.model.Indicator;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

public class IndicatorProvider {

    public List<Indicator> getIndicatorList(String countryCode) throws Exception {
        return Arrays.asList(getData(countryCode, System.getProperty("travel.app.id.total.population")),
                getData(countryCode, System.getProperty("travel.app.id.total.gdp")));
    }

    private Indicator getData(String countryCode, String id) throws Exception {
        URI targetURI = new URI(System.getProperty("travel.app.indicator.id").
                replace("{country_code}", countryCode).replace("{id}", id));

        HttpRequest httpRequest = HttpRequest.newBuilder().uri(targetURI).GET().build();
        HttpClient httpClient = HttpClient.newHttpClient();
        HttpResponse<String> response = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());

        JsonElement jsonElement = JsonParser.parseString(response.body()).getAsJsonArray().get(1);

        Indicator indicator = new Indicator();
        indicator.setId(id);
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);

        if (jsonElement.isJsonArray()) {
            JsonArray jsonArray = jsonElement.getAsJsonArray();
            for (JsonElement element : jsonArray) {
                JsonObject object = element.getAsJsonObject();
                int year = object.get("date").getAsInt();
                if (year == currentYear - 1) {
                    indicator.setYear(year);
                    indicator.setValue(object.get("value").getAsLong());
                    break;
                }
            }
        }

        return indicator;
    }

}
