package mz.mpesa.travelapp.service.ws;

import mz.mpesa.travelapp.service.exception.IndicatorException;
import mz.mpesa.travelapp.service.model.Indicator;
import mz.mpesa.travelapp.service.provider.IndicatorProvider;

import javax.inject.Inject;
import java.util.List;

public class IndicatorServiceImpl implements IndicatorService {

    @Inject
    private IndicatorProvider indicatorProvider;

    @Override
    public List<Indicator> listIndicators(String countryCode) {
        try {
            return this.indicatorProvider.getIndicatorList(countryCode);
        } catch (Exception exception) {
            throw new IndicatorException("Error occurred while getting indicator data from provider API", exception);
        }
    }
}
