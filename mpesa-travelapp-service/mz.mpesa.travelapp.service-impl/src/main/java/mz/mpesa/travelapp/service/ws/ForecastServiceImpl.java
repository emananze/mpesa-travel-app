package mz.mpesa.travelapp.service.ws;

import mz.mpesa.travelapp.service.exception.ForecastException;
import mz.mpesa.travelapp.service.model.forecast.CityForecast;
import mz.mpesa.travelapp.service.provider.ForecastProvider;

import javax.inject.Inject;

public class ForecastServiceImpl implements ForecastService {

    @Inject
    private ForecastProvider forecastProvider;

    @Override
    public CityForecast getCityForecast(String city) {
        try {
            return this.forecastProvider.getForecast(city);
        } catch (Exception exception) {
            throw new ForecastException("Error occurred while getting forecast data from provider API", exception);
        }
    }
}
