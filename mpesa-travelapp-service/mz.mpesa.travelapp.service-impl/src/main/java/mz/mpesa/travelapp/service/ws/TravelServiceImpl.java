package mz.mpesa.travelapp.service.ws;

import mz.mpesa.travelapp.service.exception.TravelServiceException;
import mz.mpesa.travelapp.service.model.CityTravelDetail;
import mz.mpesa.travelapp.service.model.Currency;
import mz.mpesa.travelapp.service.model.Indicator;
import mz.mpesa.travelapp.service.model.forecast.CityForecast;

import javax.inject.Inject;
import java.util.List;

public class TravelServiceImpl implements TravelService {

    @Inject
    private ForecastService forecastService;
    @Inject
    private CurrencyService currencyService;
    @Inject
    private IndicatorService indicatorService;

    @Override
    public CityTravelDetail getCityTravelDetail(String cityName) {
        try {
            CityTravelDetail travelDetail = new CityTravelDetail();

            CityForecast forecast = this.forecastService.getCityForecast(cityName);
            travelDetail.setForecast(forecast);

            Currency currency = this.currencyService.getCurrency(forecast.getSys().getCountry());
            travelDetail.setCurrency(currency);

            List<Indicator> indicators = this.indicatorService.listIndicators(forecast.getSys().getCountry());
            travelDetail.setIndicators(indicators);

            return travelDetail;
        } catch (Exception exception) {
            throw new TravelServiceException("Error occurred while getting travel data", exception);
        }
    }
}
