package mz.mpesa.travelapp.service.ws;

import mz.mpesa.travelapp.service.model.Currency;
import mz.mpesa.travelapp.service.utils.BaseTestClass;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;

class CurrencyServiceImplTest extends BaseTestClass {

    @Inject
    private CurrencyService currencyService;

    @Test
    void getCurrency() {
        Currency currency = this.currencyService.getCurrency("USA");
        Assertions.assertNotNull(currency);
    }

}