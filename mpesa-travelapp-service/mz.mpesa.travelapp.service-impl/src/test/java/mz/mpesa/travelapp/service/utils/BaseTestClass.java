package mz.mpesa.travelapp.service.utils;

import org.jboss.weld.junit5.EnableWeld;
import org.jboss.weld.junit5.WeldInitiator;
import org.jboss.weld.junit5.WeldSetup;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;

@EnableWeld
public class BaseTestClass {

    @WeldSetup
    public WeldInitiator weld = WeldInitiator.from(WeldInitiator.createWeld()
            .enableDiscovery()).activate(SessionScoped.class, ApplicationScoped.class).build();

    static {
        System.setProperty("travel.app.forecast.url", "https://api.openweathermap.org/data/2.5/weather?q={cityName}&APPID=5b8e50abd5955638cfb81c8572d936e0");
        System.setProperty("travel.app.currency.by.country", "https://restcountries.com/v3.1/alpha/{country-code}");
        System.setProperty("travel.app.currency.exchange", "http://api.exchangeratesapi.io/v1/{current_date}?access_key=3fdbb8300f8fe2cd958fb541462e89bc&base=EUR&symbols={currency_code}");
        System.setProperty("travel.app.indicator.id", "https://api.worldbank.org/v2/country/{country_code}/indicator/{id}?format=json");
        System.setProperty("travel.app.id.total.population", "SP.POP.TOTL");
        System.setProperty("travel.app.id.total.gdp", "NY.GDP.PCAP.CD");
    }

}