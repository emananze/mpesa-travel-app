package mz.mpesa.travelapp.service.ws;


import mz.mpesa.travelapp.service.model.Indicator;
import mz.mpesa.travelapp.service.utils.BaseTestClass;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.util.List;

class IndicatorServiceImplTest extends BaseTestClass {

    @Inject
    private IndicatorService indicatorService;

    @Test
    void listIndicators() {
        List<Indicator> indicators = this.indicatorService.listIndicators("USA");
        Assertions.assertFalse(indicators.isEmpty());
    }

}