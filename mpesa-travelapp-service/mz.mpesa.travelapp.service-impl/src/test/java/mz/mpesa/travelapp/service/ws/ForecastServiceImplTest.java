package mz.mpesa.travelapp.service.ws;

import mz.mpesa.travelapp.service.model.forecast.CityForecast;
import mz.mpesa.travelapp.service.utils.BaseTestClass;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;

class ForecastServiceImplTest extends BaseTestClass {

    @Inject
    private ForecastService forecastService;

    @Test
    void getCityForecast() {
        CityForecast forecast = this.forecastService.getCityForecast("liverpool");
        Assertions.assertNotNull(forecast);
    }

}