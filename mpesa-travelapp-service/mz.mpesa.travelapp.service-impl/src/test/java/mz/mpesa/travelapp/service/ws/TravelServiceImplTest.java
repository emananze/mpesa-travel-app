package mz.mpesa.travelapp.service.ws;

import mz.mpesa.travelapp.service.model.CityTravelDetail;
import mz.mpesa.travelapp.service.utils.BaseTestClass;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;

class TravelServiceImplTest extends BaseTestClass {

    @Inject
    private TravelService travelService;

    @Test
    void getCityTravelDetail() {
        CityTravelDetail detail = this.travelService.getCityTravelDetail("liverpool");
        Assertions.assertNotNull(detail);
    }

}