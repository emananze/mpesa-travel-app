import { Box, Center, Heading } from "@chakra-ui/react";

const NavBar = () => {
  return (
    <Box bg="gray.700" w="100vw" p={4} color="white">
      <Center>
        <Heading>Mpesa Travel Application</Heading>
      </Center>
    </Box>
  );
};

export default NavBar;
