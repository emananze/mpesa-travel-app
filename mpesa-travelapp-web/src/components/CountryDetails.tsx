import { FC } from "react";
import { Box, Text } from "@chakra-ui/react";

export interface Country {
  name: string;
  population: number;
  gdpPerCapita: number;
}

interface CountryDetailsProps {
  country: Country;
}

const CountryDetails: FC<CountryDetailsProps> = ({ country }) => {
  return (
    <Box w="100vw" p={4} color="white">
      <Text>Country: {country.name}</Text>
      <Text>Population: {country.population}</Text>
      <Text>GDP Per Capita: {country.gdpPerCapita}</Text>
    </Box>
  );
};

export default CountryDetails;
