import React, { FC, useState } from "react";
import { Button, HStack, Input } from "@chakra-ui/react";

interface SearchBarProps {
  onSearch: (searchTerm: string) => void;
}

const SearchBar: FC<SearchBarProps> = ({ onSearch }) => {
  const [searchTerm, setSearchTerm] = useState("");

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    onSearch(searchTerm);
  };

  return (
    <form onSubmit={handleSubmit}>
      <HStack spacing={4} mb={4}>
        <Input
          type="text"
          placeholder="Search for a city..."
          value={searchTerm}
          onChange={(e) => setSearchTerm(e.target.value)}
        />
        <Button type="submit" colorScheme="teal">
          Search
        </Button>
      </HStack>
    </form>
  );
};

export default SearchBar;
