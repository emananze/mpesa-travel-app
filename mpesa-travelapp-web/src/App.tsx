import NavBar from "./components/NavBar.tsx";
import { Container } from "@chakra-ui/react";
import SearchBar from "./components/SearchBar.tsx";
import { useState } from "react";
import CountryDetails, { Country } from "./components/CountryDetails.tsx";

function App() {
  const [country, setCountry] = useState<Country | null>(null);

  const handleSearch = async (searchTerm: string) => {
    const response = await fetch(`https://apiurl/${searchTerm}`);
    const data = await response.json();

    setCountry({
      name: data[0].name.common,
      population: data[0].population,
      gdpPerCapita: 0, // To modify
    });
  };

  return (
    <>
      <header>
        <NavBar />
      </header>
      <Container mt={4}>
        <SearchBar onSearch={handleSearch} />
        {country && <CountryDetails country={country} />}
      </Container>
    </>
  );
}

export default App;
